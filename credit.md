# Credits

The list below of contributors, direct or indirect, to the deconstruction of *F-Zero AX* and *F-Zero GX* files.

* **Raphaël Tétreault (StarkNebula)**
  * COLI_COURSE structures
  * GMA structures
  * GX VAT formats
  * livecam_stage structures



TODO:

* CHMCL: 
* CosmoCortney: 
* GameZelda: https://forum.xentax.com/viewtopic.php?t=6812
* Yoshifan28: 
* Others I can't think of at the moment