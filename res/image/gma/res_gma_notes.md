# Resources GMA Notes

## effective_model

Images of objects which are labelled as "Effective Model". These are objects that are affected by physics.

## is_swappable_texture

Images of most/every object in F-Zero GX which has the GMA TextureDescriptor 0x0D IsSwappableTexture as variable set as true.