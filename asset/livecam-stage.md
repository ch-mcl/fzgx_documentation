## Revisions

| Author            | Date       | Version | Description      |
| ----------------- | ---------- | ------- | ---------------- |
| Raphaël Tétreault | 2020/05/17 | 1.0     | Initial document |



## Table of Contents

[TOC]

# Live Camera Stage

The `livecam_stage*` files contain an array of camera pan instructions/parameters to be played either at the start of a stage (`livecam_stage`) or at the end of the attract mode sequence (`livecam_stage_demo`). They appear to have secondary uses as well; some parts of their data (needing further investigation) is repurposed as camera data during replays. The specifics of how this occurs and what is used is currently unknown.

# Structures

Before diving in, it is important to note that the `livecam_stage*` contents are an arbitrarily large array of a single structure type. The game solves how many items are in the array on it's own. This breakdown will go over the structure. It is up to the reader to understand how serialization of the array should look like.

## Camera Pan

This container has all the necessary information for a single camera pan. As reference, most intro sequences to *F-Zero GX* use 7 pans in sequence, while another few use 9. Story Mode missions use 1, as do AX stages. However, the AX stage camera pan data is repurposed demo camera pans, of which there is always 3. The table in [Notes](#notes) outlines the specifics used for these parameters.

A description of how to interpolate can be found in [Notes](#notes).

| Offset | Type             | Var Name    | Description                                |
| ------ | ---------------- | ----------- | ------------------------------------------ |
| 0x00   | int32            | Frame Count | The number of frames this pan lasts.       |
| 0x04   | float            | Lerp Speed  | The time value used in interpolating pans. |
| 0x08   | int32            | Zero 0x08   | 4 bytes, always 0.                         |
| 0x1C   | Camera Pan Point | From        | The values to interpolate from.            |
| 0x40   | Camera Pan Point | To          | The values to interpolate to.              |

## Camera Pan Point

| Offset | Type   | Var Name                 | Description                                                  |
| ------ | ------ | ------------------------ | ------------------------------------------------------------ |
| 0x00   | float3 | Camera Position          | The camera's from/to position.                               |
| 0x0C   | float3 | Lookat Position          | The camera's from/to lookat target position.                 |
| 0x18   | float  | FOV                      | The camera's field-of-view.                                  |
| 0x1C   | int16  | Rotation                 | The camera's rotation around the camera's forward axis. See [Camera Pan Rotation](#camera-pan-rotation). |
| 0x1E   | int16  | Zero 0x1E                | 2 bytes, always 0.                                           |
| 0x20   | enum16 | Camera Pan Interpolation | The interpolation method to use Ignored on FROM parameter.   |
| 0x22   | int16  | Zero 0x22                | 2  bytes, always 0.                                          |

## Camera Pan Rotation

The camera pan rotation is a signed 16-bit integer which has an absolute range of 180 degrees clockwise (exclusive) through -180 degrees counter-clockwise (inclusive). The rotation is performed relative to the camera's forward normal.

To convert a the value from short to float and vice-versa, use a reciprocal value. The samples below in C# syntax demonstrate:

**Convert from int16 to float**

```C#
const float convert_S16_F32 = 180f / (short.MaxValue + 1);
// 90 degrees CW
const short exRotation = (short.maxValue + 1) / 2;
// 90 degrees CW as float
float rotationF32 = exRotation * convert_S16_F32;
```

**Convert from float to int16**

```C#
const float convert_F32_S16 = (short.MaxValue + 1) / 180f;
// 90 degrees CW
const short exRotation = (short.maxValue + 1) / 2;
// 90 degrees CW as float
float rotationF32 = exRotation * convert_F32_S16;
```

## Camera Pan Interpolation

These values are used exclusively by the TO parameter and is ignored by the FROM parameter. They appear to indicate the interpolation method. The video [GFZX01 livecam_stage Camera Pan Mode by StarkNebula](https://youtu.be/beCt3pVWHtY) demonstrates the available properties and their respective output.

Note that values 0, 2, 3 are only ever present in these files. 1 is listed as it may have been implemented at one point. The value 1 should not be used.

| Index | Description                                             |
| ----- | ------------------------------------------------------- |
| 0     | LERP: linear interpolation                              |
| 1     | Unused/undefined: there is no interpolation whatsoever. |
| 2     | Ease-Out: fast start, slow down.                        |
| 3     | Ease-In-Out: slow to start, fast middle, slow end       |

# Notes

## Interpolations Method

If you are unfamiliar with LERPing, you can referenced Wikipedia's [Linear interpolation](https://en.wikipedia.org/wiki/Linear_interpolation) page.

The camera pan does some interpolation between the FROM and TO parameters using a simple technique. The camera is initialized to `From.CameraPosition` and it's rotation is set to face the `From.LookatPosition`.  Math libraries dealing with quaternions, such as Unity, has methods named `LookatRotation` which creates a rotation based on 2 vectors (creating the third with a cross-product). The forward vector is the vector from the camera's position to the lookat target, the Up vector is the global Up vector. After this, the rotation is applied around the forward vector of the camera. Finally, the camera's field-of-view is set to `fov`.

Each frame as defined by `FrameCount`, the stored variables for camera position, lookat target, and fov are updated to the LERPed value of itself with the `To` parameters using the `LerpSpeed` parameter. This new value is stored in the variable and used in the next frame's iteration. This has the effect of slowly decelerating in the interpolation, smoothing it out.

The `interpolation` parameter of the `To` camera pan point is used to modify the feel and isn't 100% worked out yet. There is a video linked in [Camera Pan Interpolation](#camera-pan-interpolation) as reference.

Overall, a C# implementation would look something like below.

```C#
// Create variables to store lerped value
var cameraPos = pan.from.cameraPosition;
var lookatPos = pan.from.lookatPosition;
var fov = pan.from.fov;

// Iterate over the camera pan's frames
for (int frame = 0; frame < pan.frameCount; frame++)
{
    // Lerp values
    cameraPos = Lerp(cameraPos, pan.to.cameraPosition, pan.lerpSpeed);
    lookatPos = Lerp(lookatPos, pan.to.lookatPosition, pan.lerpSpeed);
    fov = Lerp(fov, pan.to.fov, pan.lerpSpeed);

    // Set camera values
    camera.transform.position = cameraPos;
    camera.transform.LookAt(lookatPos, Vector3.up);
    camera.fieldOfView = fov;
}
```

## Expected Values

This section denotes the values most often seen for these parameters.

### FOV

FOV ranges from 23 to 108 at the extreme, but averages 60 overall.

### Frame Durations

#### F-Zero GX Standard

The standard intro camera pans use the following style.

| Index | Frame Count |
| ----- | ----------- |
| 1     | 142         |
| 2     | 50          |
| 3     | 89          |
| 4     | 136         |
| 5     | 136         |
| 6     | 136         |
| 7     | 150         |

#### F-Zero GX Alternate

The alternate format for intro camera pans use 9 shots instead of 6 by splitting up pans 2 and 3 from [F-Zero GX Standard](#F-Zero GX Standard) into smaller chunks for added effect. 17 frames x 16.67ms = 283ms, which is just barely enough for a human to react to.

| Index | Frame Count |
| ----- | ----------- |
| 1     | 142         |
| 2     | 17          |
| 3     | 33          |
| 4     | 17          |
| 5     | 72          |
| 6     | 136         |
| 7     | 136         |
| 8     | 136         |
| 9     | 150         |

#### F-Zero AX/GX Demo

Used in the attract mode of both games, the `livecam_stage_demo*` files use 3 camera pans with the following time signatures.

| Index | Frame Count |
| ----- | ----------- |
| 1     | 350         |
| 2     | 500         |
| 3     | 350         |

#### F-Zero GX Story / AX Stages

The story mode intros in F-Zero GX and the AX course intros in both games use a different stinger that is shorter. These only include a single pan over about 7 seconds. Of note, F-Zero GX repurposes the F-Zero AX demo intros for this purpose. The intro for stages 31-36 are all hard-coded to end once the stinger completes instead of waiting for the camera pans to complete.

| Index | Frame Count |
| ----- | ----------- |
| 1     | 350         |

### Interpolation

The [interpolation](#Camera Pan Interpolation) method most often seen for the TO parameter is 2. The FROM parameter is always 0.

