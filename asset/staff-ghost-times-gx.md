# GX Staff Ghost

This sheet logs the staff ghosts used in F-Zero GX. Sorting order is by stage index number.

## Time Attack Ghosts

| #    | Cup      | Stage          | Track           | Machine          | Time     | Name   |
| ---- | -------- | -------------- | --------------- | ---------------- | -------- | ------ |
| 1    | Ruby     | Mute City      | Twist Road      | Death Anchor     | 0'59"964 | FZ     |
| 3    | Sapphire | Mute City      | Serial Gaps     | Night Thunder    | 1"26'549 | FZ     |
| 5    | Ruby     | Aeropolis      | Multiplex       | Fire Stingray    | 2'15"297 | FZ     |
| 7    | Sapphire | Port Town      | Aero Dive       | Black Bull       | 2'25"689 | FZ     |
| 8    | Sapphire | Lightning      | Loop Cross      | Wild Boar        | 1'59"907 | FZ     |
| 9    | Emerald  | Lightning      | Half Pipe       | Sonic Phantom    | 2'50"296 | FZ     |
| 10   | Emerald  | Green Plant    | Intersection    | Mighty Hurricane | 2'24"897 | FZ     |
| 11   | Sapphire | Green Plant    | Mobius Ring     | Death Anchor     | 1'35"169 | FZ     |
| 13   | Sapphire | Port Town      | Long Pipe       | Wild Boar        | 2'26"802 | FZ     |
| 14   | Sapphire | Big Blue       | Drift Highway   | Night Thunder    | 1'07"331 | FZ     |
| 15   | Emerald  | Fire Field     | Cylinder Knot   | Black Bull       | 2'40"909 | FZ     |
| 16   | Ruby     | Casino Palace  | Split Oval      | Black Bull       | 0'56"691 | FZ     |
| 17   | Diamond  | Fire Field     | Undulation      | Twin Noritta     | 2'06"916 | FZ     |
| 21   | Diamond  | Aeropolis      | Dragon Slope    | Death Anchor     | 2'59"076 | FZ     |
| 24   | Diamond  | Cosmo Terminal | Trident         | Black Bull       | 2'58"650 | FZ     |
| 25   | Diamond  | Sand Ocean     | Lateral Shift   | Hyper Speeder    | 2'09"849 | FZ     |
| 26   | Ruby     | Sand Ocean     | Surface Slide   | Big Fang         | 1'56"743 | FZ     |
| 27   | Emerald  | Big Blue       | Ordeal          | Fire Stingray    | 2'50"808 | FZ     |
| 28   | Diamond  | Phantom Road   | Slim-Line Slits | Queen Meteor     | 2'03"268 | FZ     |
| 29   | Emerald  | Casino Palace  | Double Branches | Fire Stingray    | 2'59"117 | FZ     |
| 31   | AX       | Aeropolis      | Screw Drive     | Rolling Turtle   | 1'06"690 | FZ     |
| 32   | AX       | Outer Space    | Meteor Stream   | Wild Goose       | 1'38"178 | FZ     |
| 33   | AX       | Port Town      | Cylinder Wave   | Fat Shark        | 2'06"837 | 開発者 |
| 34   | AX       | Lightning      | Thunder Road    | Fire Stingray    | 3'18"272 | FZ     |
| 35   | AX       | Green Plant    | Spiral          | Rainbow Phoenix  | 3'50"740 | FZ     |
| 36   | AX       | Mute City      | Sonic Oval      | Fat Shark        | 0'46"893 | FZ     |

## Story Ghosts

Story Mode mission 9 "Finale: Enter the Creators" uses ghost data for The Creator's machine. The times are never explicitly shown to the player, but are quantified in the data used. 

| Stage Index | Ghost Index | Difficulty | Machine     | Time     | Name |
| ----------- | ----------- | ---------- | ----------- | -------- | ---- |
| 45          | 00          | Normal     | Blue Falcon | 2'32"810 | FZ   |
| 45          | 01          | Hard       | Blue Falcon | 2'25"073 | FZ   |
| 45          | 02          | Very Hard  | Blue Falcon | 2'20"176 | FZ   |

