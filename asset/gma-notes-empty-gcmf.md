# Empty GCMF

A GCMF entry can be present without the need of other attributes. (Min size: 0x40)

![EmptyGCMF](..\res\image\gma\empty_gcmf.png)

This model is called `pos_29_crazybear_a` which would seem to indicate that is an empty node for positional reference. No materials. No Textures. Nothing.

