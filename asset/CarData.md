# F-Zero GX CarData



## Revisions  Table

| Author            | Date       | Version | Description      |
| ----------------- | ---------- | ------- | ---------------- |
| Raphaël Tétreault | 2019/05/08 | 1.0     | Initial document |



## Table Of Contents

[TOC]

# Introduction

*root/game/cardata.lz* is the archive that houses the vehicle stats for every machine and custom part in *<u>F-Zero GX</u>*. *<u>F-Zero AX</u>* does not include this file.

The file is LZ compressed. This file assumes you have the files decompressed with GXPAND or similar tools for inspection.

# Structures

**Endianness**

The *cardata* file and it's structures are serialized in Big Endian.

**String Encoding**

Strings are encoded using ASCII/ANSI (8-bit characters) in C-String format (0x00 null terminated).

## CarData

This represents the uncompressed *cardata* file.

| Offset | Type                                                 | Var Name           | Description                                                  |
| ------ | ---------------------------------------------------- | ------------------ | ------------------------------------------------------------ |
| 0x0000 | [Vehicle Parameters](#vehicle-parameters) Array [41] | Machines           | All 41 machine stats. See [Machines](#machines) for serialization order. |
| 0x1CE0 | C String Array [41]                                  | Machine Name Table | All 41 machine names. See [Machine Name Table](#machine-name-table) for serialization order. |
| 0x1EDE | [CarData Padding](#cardata-padding)                  | Padding 0x1EDE     | Padding data. See [CarData Padding](#cardata-padding).       |
| 0x1F04 | [Vehicle Parameters](#vehicle-parameters) Array [25] | Body Parts         | All 25 body parts. See [Body Parts](#body-parts).            |
| 0x3098 | [Vehicle Parameters](#vehicle-parameters) Array [25] | Cockpit Parts      | All 25 cockpit parts. See [Cockpit Parts](#cockpit-parts).   |
| 0x422C | [Vehicle Parameters](#vehicle-parameters) Array [25] | Booster Parts      | All 25 booster parts. See [Booster Parts](#booster-parts).   |
| 0x53C0 | C String Table [30]                                  | Unknown Name Table | An unknown name table with 30 entries. See [Unknown Name Table](#unknown-name-table) for values and serialization order. |
| 0x5474 | [CarData Padding](#cardata-padding)                  | Padding 0x5474     | Padding data. See [CarData Padding](#cardata-padding).       |



## Vehicle Parameters

Each machine has a structure like this associated to it. 

### Custom Parts

Custom parts stats are combined at runtime to create a whole machine. However, not all stats are combined. Here is a table outlining what kinds of operations are used to create a custom machine.

| Mnemonic                    | Description                                                 |
| --------------------------- | ----------------------------------------------------------- |
| Custom-Body/Cockpit/Booster | Uses only value from Body, Cockpit, or Booster.             |
| Custom-Additive             | Combines stats from all parts.                              |
| Custom-Multiply-BC          | Multiplies stats from Body and Cockpit.                     |
| Custom-Min-Max*             | Uses the min/max value from Body, Cockpit, or Booster. *TBC |

With that in mind, here's the data structure:

| Offset | Type                                                         | Var Name                  | Description                                                  |
| ------ | ------------------------------------------------------------ | ------------------------- | ------------------------------------------------------------ |
| 0x00   | uint                                                         | Name Pointer              | Replaced at runtime by a pointer pointing to the vehicle's name |
| 0x04   | float                                                        | Weight                    | Custom-Additive.                                             |
| 0x08   | float                                                        | Acceleration              | Custom-Booster.                                              |
| 0x0C   | float                                                        | Max Speed                 | Custom-Booster.                                              |
| 0x10   | float                                                        | Grip 1                    | Custom-Body.                                                 |
| 0x14   | float                                                        | Grip 3                    | Custom-Body.                                                 |
| 0x18   | float                                                        | Turn Tension              | Custom-Body.                                                 |
| 0x1C   | float                                                        | Drift Acceleration        | Custom-Booster.                                              |
| 0x20   | float                                                        | Turn Movement             | Custom-Cockpit.                                              |
| 0x24   | float                                                        | Strafe Turn               | Custom-Cockpit.                                              |
| 0x28   | float                                                        | Strafe                    | Custom-Body.                                                 |
| 0x2C   | float                                                        | Turn Reaction             | Custom-Body.                                                 |
| 0x30   | float                                                        | Grip 2                    | Custom-Cockpit.                                              |
| 0x34   | float                                                        | Boost Strength            | Custom-Booster.                                              |
| 0x38   | float                                                        | Boost Duration            | Custom-Booster.                                              |
| 0x3C   | float                                                        | Turn Deceleration         | Custom-Booster.                                              |
| 0x40   | float                                                        | Drag                      | Custom-Booster.                                              |
| 0x44   | float                                                        | Body                      | Custom-Multiply-BC. Defense stat.                            |
| 0x48   | [Vehicle Parameters Flags 0x48](#vehicle-parameters-flags-0x48) | Unknown 0x48              | Custom-Cockpit.                                              |
| 0x49   | [Vehicle Parameters Flags 0x49](#vehicle-parameters-flags-0x49) | Unknown 0x49              |                                                              |
| 0x4A   | uint16                                                       | Zero 0x4A                 |                                                              |
| 0x4C   | float                                                        | Camera Reorientation      | Custom-Cockpit.                                              |
| 0x50   | float                                                        | Camera Repositioning      | Custom-Cockpit.                                              |
| 0x54   | float3                                                       | Tilt Front Right          | Custom-Body.                                                 |
| 0x60   | float3                                                       | Tilt Front Left           | Custom-Body.                                                 |
| 0x6C   | float3                                                       | Tilt Back Right           | Custom-Body.                                                 |
| 0x78   | float3                                                       | Tilt Back Left            | Custom-Body.                                                 |
| 0x84   | float3                                                       | Collision Box Front Right | Custom-Min-Max*.                                             |
| 0x90   | float3                                                       | Collision Box Front Left  | Custom-Min-Max*.                                             |
| 0x9C   | float3                                                       | Collision Box Back Right  | Custom-Min-Max*.                                             |
| 0x108  | float3                                                       | Collision Box Back Left   | Custom-Min-Max*.                                             |

### Vehicle Parameters Flags 0x48

This structure has not been thoroughly tested but is believed to control the camera. Some vehicles like Wild Goose have a camera that *always* follows *directly* behind the vehicle, even when drifting. I (StarkNebula) believe these bits to inform those camera behaviours.

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | Unknown 1 |             |
| 1    | Unknown 2 |             |
| 2    | Unknown 3 |             |
| 3    | Unknown 4 |             |
| 4    | unused    |             |
| 5    | unused    |             |
| 6    | unused    |             |
| 7    | unused    |             |

### Vehicle Parameters Flags 0x49

*This may not be flags, but since there are 4 unique values (0 through 3) it looks like it. They only have non-zero values on machines and not on custom parts.

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | Unknown 1 |             |
| 1    | Unknown 2 |             |
| 2    | unused    |             |
| 3    | unused    |             |
| 4    | unused    |             |
| 5    | unused    |             |
| 6    | unused    |             |
| 7    | unused    |             |



## CarData Padding

This appears to be a padding structure 0x26 bytes long. It begins with a 0x00, followed by 0x24 numeric characters ('0' through '9' on loop), and a final 0x00. This could be seen as an empty C-style string and a numeric string for padding.

| Byte Array Representation                                    |
| ------------------------------------------------------------ |
| 0x00, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x00 |



# Vehicle Parameter Order

## Machines

There are 41 machines in total numbered 0-40. However, the serialization order is not the same as their Machine index order. The *<u>F-Zero X</u>* cast is serialized first, followed by the *<u>F-Zero GX</u>* boss' machine Dark Schneider, proceeded then by the *<u>F-Zero AX</u>* cast. This order is referred to as the *Serialization Order*.

| Serialization Order | Machine Index | Machine Name     |
| ------------------- | ------------- | ---------------- |
| 0                   | 1             | Red Gazelle      |
| 1                   | 2             | White Cat        |
| 2                   | 3             | Golden Fox       |
| 3                   | 4             | Iron Tiger       |
| 4                   | 5             | Fire Stingray    |
| 5                   | 6             | Wild Goose       |
| 6                   | 7             | Blue Falcon      |
| 7                   | 8             | Deep Claw        |
| 8                   | 9             | Great Star       |
| 9                   | 10            | Little Wyvern    |
| 10                  | 11            | Mad Wolf         |
| 11                  | 12            | Super Piranha    |
| 12                  | 13            | Death Anchor     |
| 13                  | 14            | Astro Robin      |
| 14                  | 15            | Big Fang         |
| 15                  | 16            | Sonic Phantom    |
| 16                  | 17            | Green Panther    |
| 17                  | 18            | Hyper Speeder    |
| 18                  | 19            | Space Angler     |
| 19                  | 20            | King Meteor      |
| 20                  | 21            | Queen Meteor     |
| 21                  | 22            | Twin Noritta     |
| 22                  | 23            | Night Thunder    |
| 23                  | 24            | Wild Boar        |
| 24                  | 25            | Blood Hawk       |
| 25                  | 26            | Wonder Wasp      |
| 26                  | 27            | Mighty Typhoon   |
| 27                  | 28            | Mighty Hurricane |
| 28                  | 29            | Crazy Bear       |
| 29                  | 30            | Black Bull       |
| 30                  | 0             | Dark Schneider   |
| 31                  | 31            | Fat Shark        |
| 32                  | 32            | Cosmic Dolphin   |
| 33                  | 33            | Pink Spider      |
| 34                  | 34            | Magic Seagull    |
| 35                  | 35            | Silver Rat       |
| 36                  | 36            | Spark Moon       |
| 37                  | 37            | Bunny Flash      |
| 38                  | 38            | Groovy Taxi      |
| 39                  | 39            | Rolling Turtle   |
| 40                  | 40            | Rainbow Phoenix  |

## Body Parts

Custom parts are serialized in index order.

| Serialization Order | Part Index | Part Name      |
| ------------------- | ---------- | -------------- |
| 0                   | 1          | Aqua Goose     |
| 1                   | 2          | Big Tyrant     |
| 2                   | 3          | Blood Raven    |
| 3                   | 4          | Brave Eagle    |
| 4                   | 5          | Dread Hammer   |
| 5                   | 6          | Fire Wolf      |
| 6                   | 7          | Funny Swallow  |
| 7                   | 8          | Galaxy Falcon  |
| 8                   | 9          | Giant Planet   |
| 9                   | 10         | Grand Base     |
| 10                  | 11         | Holy Spider    |
| 11                  | 12         | Liberty Manta  |
| 12                  | 13         | Mad Bull       |
| 13                  | 14         | Megalo Cruiser |
| 14                  | 15         | Metal Shell    |
| 15                  | 16         | Optical Wing   |
| 16                  | 17         | Rage Knight    |
| 17                  | 18         | Rapid Barrel   |
| 18                  | 19         | Silver Sword   |
| 19                  | 20         | Sky Horse      |
| 20                  | 21         | Space Cancer   |
| 21                  | 22         | Speedy Dragon  |
| 22                  | 23         | Splash Whale   |
| 23                  | 24         | Valiant Jaguar |
| 24                  | 25         | Wild Chariot   |

## Cockpit Parts

Custom parts are serialized in index order.

| Serialization Order | Part Index | Part Name      |
| ------------------- | ---------- | -------------- |
| 0                   | 1          | Aerial Bullet  |
| 1                   | 2          | Blast Camel    |
| 2                   | 3          | Bright Spear   |
| 3                   | 4          | Combat Cannon  |
| 4                   | 5          | Crazy Buffalo  |
| 5                   | 6          | Crystal Egg    |
| 6                   | 7          | Cyber Fox      |
| 7                   | 8          | Dark Chaser    |
| 8                   | 9          | Energy Crest   |
| 9                   | 10         | Garnet Phantom |
| 10                  | 11         | Heat Snake     |
| 11                  | 12         | Hyper Stream   |
| 12                  | 13         | Maximum Star   |
| 13                  | 14         | Moon Snail     |
| 14                  | 15         | Muscle Gorilla |
| 15                  | 16         | Rave Drifter   |
| 16                  | 17         | Red Rex        |
| 17                  | 18         | Round Disk     |
| 18                  | 19         | Rush Cyclone   |
| 19                  | 20         | Scud Viper     |
| 20                  | 21         | Sonic Soldier  |
| 21                  | 22         | Spark Bird     |
| 22                  | 23         | Super Lynx     |
| 23                  | 24         | Windy Shark    |
| 24                  | 25         | Wonder Worm    |

## Booster Parts

Custom parts are serialized in index order.

| Serialization Order | Part Index | Part Name       |
| ------------------- | ---------- | --------------- |
| 0                   | 1          | Bazooka -YS     |
| 1                   | 2          | Bluster -X      |
| 2                   | 3          | Boxer -2C       |
| 3                   | 4          | Comet -V        |
| 4                   | 5          | Crown -77       |
| 5                   | 6          | Devilfish -RX   |
| 6                   | 7          | Euros -01       |
| 7                   | 8          | Extreme -ZZ     |
| 8                   | 9          | Hornet -FX      |
| 9                   | 10         | Impulse 220     |
| 10                  | 11         | Jupiter -Q      |
| 11                  | 12         | Mars -EX        |
| 12                  | 13         | Meteor -RR      |
| 13                  | 14         | Punisher -4X    |
| 14                  | 15         | Raiden -88      |
| 15                  | 16         | Saturn -SG      |
| 16                  | 17         | Scorpion -R     |
| 17                  | 18         | Shuttle -M2     |
| 18                  | 19         | Sunrise 140     |
| 19                  | 20         | Thunderbolt -V2 |
| 20                  | 21         | Tiger -RZ       |
| 21                  | 22         | Titan -G4       |
| 22                  | 23         | Triangle -GT    |
| 23                  | 24         | Triple -Z       |
| 24                  | 25         | Velocity -J     |



# String Tables

## Machine Name Table

Every machine's name is serialized in reverse serialization index order, starting with Rainbow Phoenix through to Dark Schneider then through to Red Gazelle.

| Serialization Order | Machine Index | Machine Name     |
| ------------------- | ------------- | ---------------- |
| 0                   | 40            | Rainbow Phoenix  |
| 1                   | 39            | Rolling Turtle   |
| 2                   | 38            | Groovy Taxi      |
| 3                   | 37            | Bunny Flash      |
| 4                   | 36            | Spark Moon       |
| 5                   | 35            | Silver Rat       |
| 6                   | 34            | Magic Seagull    |
| 7                   | 33            | Pink Spider      |
| 8                   | 32            | Cosmic Dolphin   |
| 9                   | 31            | Fat Shark        |
| 10                  | 0             | Dark Schneider   |
| 11                  | 30            | Black Bull       |
| 12                  | 29            | Crazy Bear       |
| 13                  | 28            | Mighty Hurricane |
| 14                  | 27            | Mighty Typhoon   |
| 15                  | 26            | Wonder Wasp      |
| 16                  | 25            | Blood Hawk       |
| 17                  | 24            | Wild Boar        |
| 18                  | 23            | Night Thunder    |
| 19                  | 22            | Twin Noritta     |
| 20                  | 21            | Queen Meteor     |
| 21                  | 20            | King Meteor      |
| 22                  | 19            | Space Angler     |
| 23                  | 18            | Hyper Speeder    |
| 24                  | 17            | Green Panther    |
| 25                  | 16            | Sonic Phantom    |
| 26                  | 15            | Big Fang         |
| 27                  | 14            | Astro Robin      |
| 28                  | 13            | Death Anchor     |
| 29                  | 12            | Super Piranha    |
| 30                  | 11            | Mad Wolf         |
| 31                  | 10            | Little Wyvern    |
| 32                  | 9             | Great Star       |
| 33                  | 8             | Deep Claw        |
| 34                  | 7             | Blue Falcon      |
| 35                  | 6             | Wild Goose       |
| 36                  | 5             | Fire Stingray    |
| 37                  | 4             | Iron Tiger       |
| 38                  | 3             | Golden Fox       |
| 39                  | 2             | White Cat        |
| 40                  | 1             | Red Gazelle      |

## Unknown Name Table

The second string table appears to be some sort of file name code. The strange thing is it *appears* to be related to the custom parts name but there are too many entries to describe sets of 3 and too few to describe each individual part. It also uses the classic notation (*<u>F-Zero GX</u>*, formerly *<u>F-Zero GC</u>* for <u>G</u>ame<u>C</u>ube and *<u>F-Zero AX</u>*, formerly *<u>F-Zero AC</u>* for <u>a</u>r<u>c</u>ade). There are 15 AC and GC names, but a different amount of alphabetic suffixes.

| #    | Serialization Order | String Value |
| ---- | ------------------- | ------------ |
| 1    | 22                  | AC-A1        |
| 2    | 10                  | AC-A2        |
| 3    | 23                  | AC-B1        |
| 4    | 3                   | AC-B2        |
| 5    | 14                  | AC-B3        |
| 6    | 20                  | AC-B4        |
| 7    | 4                   | AC-C1        |
| 8    | 9                   | AC-C2        |
| 9    | 18                  | AC-C3        |
| 10   | 26                  | AC-C4        |
| 11   | 25                  | AC-D1        |
| 12   | 15                  | AC-D2        |
| 13   | 30                  | AC-D3        |
| 14   | 21                  | AC-E1        |
| 15   | 28                  | AC-E2        |
| 16   | 13                  | GC-A1        |
| 17   | 12                  | GC-A2        |
| 18   | 29                  | GC-A3        |
| 19   | 19                  | GC-B1        |
| 20   | 11                  | GC-B2        |
| 21   | 27                  | GC-B3        |
| 22   | 5                   | GC-C1        |
| 23   | 6                   | GC-C2        |
| 24   | 17                  | GC-C3        |
| 25   | 7                   | GC-C4        |
| 26   | 16                  | GC-D1        |
| 27   | 8                   | GC-D2        |
| 28   | 1                   | GC-D3        |
| 29   | 24                  | GC-E1        |
| 30   | 2                   | GC-E2        |

| String Array in Serialization Order                          |
| ------------------------------------------------------------ |
| "GC-D3", "GC-E2", "AC-B2", "AC-C1", "GC-C1", "GC-C2", "GC-C4", "GC-D2", "AC-C2", "AC-A2", "GC-B2", "GC-A2", "GC-A1", "AC-B3", "AC-D2", "GC-D1", "GC-C3", "AC-C3", "GC-B1", "AC-B4", "AC-E1", "AC-A1", "AC-B1", "GC-E1", "AC-D1", "AC-C4", "GC-B3", "AC-E2", "GC-A3", "AC-D3" |

