# Amusement Vision GMA Format (GeoMetry Archive)

This document will cover only the GMA format used in <u>*F-Zero GX*</u> (GFZE01, GFZJ01, GFZP01). Future updates may support *<u>F-Zero AX</u>* (unverified), *<u>Super Monkey Ball</u>*, and <u>*Super Monkey Ball 2*</u>.



## Revisions Table

| Author            | Date       | Version | Description      |
| ----------------- | ---------- | ------- | ---------------- |
| Raphaël Tétreault | 2019/04/23 | 1.0     | Initial document |



## Table Of Contents

[TOC]

# Introduction

The GMA format is a one-stop-shop approach to storing geometry data for GameCube developed by Amusement Vision. It stores the model vertices, skinning/rigging information (to be confirmed), materials, and texture references (see TPL for more details).

## About this Deconstruction

There are many ways of interpreting the raw binary into something useable. The approach used in this deconstruction keeps in mind 2 constraints:

1. Object-Oriented Programming
2. Unity Engine integration

The data representation here generally follows the concept that each object houses both state and function and that the data should be viewable and (ideally) editable within the Unity Editor. As such, data is hierarchical and often times grouped into logical entities even if the structure could be streamlined further.

Of important note is that larger pieces of data are aligned to 32-bytes -- a constraint enforced by how the GameCube Graphics Processor works. **The structures outlined in this document *assume* this 32B stride delineates different data types.** 



# Structures

The GMA is comprised of the following general data structure.

* GMA
  * GCMF Pointer Pairs
  * GCMF Name Table
  * GCMF Array
    * Properties
    * Texture Description Array
    * Transform Matrix Array
    * Vertex Control Header
    * Submesh Array
      * Material
      * GX Display Lists
      * Extra Display Lists Header
      * (Extra) GX Display Lists
    * Vertex Control Type 1
    * Vertex Control Type 2
    * Vertex Control Type 3
    * Vertex Control Type 4



## GMA

The GMA is the overarching container for the archive. At it's head is an array of pointers into a name table and towards the GCMF data. This table may have nulled entries (see [`GCMF Pointer Pair`](#gcmf-pointer-pair) for more information).

**GameCube FIFO:** This structure is 32-byte aligned to be fed into the GameCube GP.

| Offset   | Type                                      | Var Name      | Description                                                  |
| -------- | ----------------------------------------- | ------------- | ------------------------------------------------------------ |
| 0x00     | `int32`                                   | GCMF Count    | How many GCMFs are in this archive (including null entries)  |
| 0x04     | `int32`                                   | Header Size   | The size of the GMA header, including FIFO padding           |
| 0x08     | [`GCMF Pointer Pair`](#gcmf-pointer-pair) | GCMF Pointers | An array of pointers to the GCMF data and GCMF name, respectively |
| variable | C `String` Array                          | GCMF Names    | An array of model names serialized using C String (null (`0x00`) terminated). |



### GCMF Pointer Pair

A pair of pointers pointing to the data and name table. This appears to serve a similar purpose to the GameCube's `GEODescriptor` type.

| Offset | Type    | Name                       | Description                                                  |
| ------ | ------- | -------------------------- | ------------------------------------------------------------ |
| 0x00   | `int32` | GCMF Data Relative Pointer | Relative pointer starting at `0x08` (GMA header size) + `GMA.GCMF_Count` * `sizeof<GCMF_Pointer_Pair>` (`0x08`). If this value is null (`0xFFFFFFFF` two's compliment `-1`), skip this entry. |
| 0x04   | `int32` | GCMF Name Relative Pointer | Relative pointer starting at `GMA.HeaderSize`. String encoding is ASCII. This is a null-terminated C string (ends read on `0x00`). |



## GCMF

| Type                          | Name                  | Description                               |
| ----------------------------- | --------------------- | ----------------------------------------- |
| [`GCMF Properties`]()         | Properties            | Model-wide properties                     |
| [`Texture Descriptor`]()      | Textures              | Array of texture descriptions for model   |
| [`GCMF Transform Matrices`]() | Transform Matrices    | Matrices for skin/stitch/effective models |
| [`Vertex Control Header`]()   | Vertex Control Header | Vertex control data pointers              |
| [`Gcmf Submesh`]()            | Submeshes             | Model vertices and material layers        |
| [`Vertex Control Type 1`]()   | Vertex Control T1     | *See type for details*                    |
| [`Vertex Control Type 2`]()   | Vertex Control T2     | *See type for details*                    |
| [`Vertex Control Type 3`]()   | Vertex Control T3     | *See type for details*                    |
| [`Vertex Control Type 4`]()   | Vertex Control T4     | *See type for details*                    |



## GCMF Properties

A GCMF is something along the lines of **G**ame**C**ube **M**odel **F**ormat. Due to the variety of data this format stores, there are many flags and "optional" data which changes how it appears when inspected.

These properties outline some of the root information that govern the GCMF as a whole. In particular are [`GCMF Attributes `](#gcmf-attributes) flags that display in the *<u>F-Zero GX</u>* model debugger which outline 16-bit fixed point numbers, stitching model information, skinned model information, and "effective" (physics-driven) model information.

**GameCube FIFO:** This structure is 32-byte aligned to be fed into the GameCube GP. This structure is aligned to `0x40` (64 bytes). There are 16 unused bytes due to this padding.

| Offset | Type                                                        | Name                      | Description                                                  |
| ------ | ----------------------------------------------------------- | ------------------------- | ------------------------------------------------------------ |
| 0x00   | `int32` / `char[4]`                                         | GCMF Magic                | "GCMF" constant in ASCII                                     |
| 0x04   | [`GCMF Attributes`](#gcmf-attributes)                       | Attributes                | GCMF attribute flags identifying type of data stored in GCMF |
| 0x08   | `float3`                                                    | Origin                    | Origin point of model                                        |
| 0x14   | `float`                                                     | Radius                    | Bounding-sphere radius of model                              |
| 0x18   | `int16`                                                     | Texture Count             | Number of texture references                                 |
| 0x1A   | `int16`                                                     | Material Count            | Number of materials                                          |
| 0x1C   | `int16`                                                     | Translucid Material Count | Number of translucid materials                               |
| 0x1E   | `int8`                                                      | Transform Matrix Count    | Number of transform matrices                                 |
| 0x1F   | `int8`                                                      | zero 0x1F                 | Constant 0                                                   |
| 0x20   | `int32`                                                     | GCMF Texture Matrix Size  | Memory size of this structure in bytes, including texture description array and transform matrices. |
| 0x24   | `uint32`                                                    | zero 0x24                 | Constant 0                                                   |
| 0x28   | [`Transform Matrix Indices 8`](#transform-matrix-indices-8) | Default Matrix Indices    | Default (root?) indices into Transform Matrix array.         |



### GCMF Attributes

*GCMF Attributes* occupy 32 bits of space but only uses the lowest 4 bits.

Each GCMF is explicitly tagged with any unique information it may be holding. While this flag is sometimes redundant (information could be loaded regardless of this flag), it is display, for instance, in the *F-Zero GX* model debugger making this tag useful for testing.

| Bit  | Name               | Description                                                  |
| ---- | ------------------ | ------------------------------------------------------------ |
| 0    | Is 16 Bit          | Vertices are stored in 16-bit compressed floating point number format using GameCube GX conventions. <span style="color:red">[**TODO**: identify which components use 16bit compression]</span> |
| 1    | `unused`           |                                                              |
| 2    | Is Stitching Model | Called "Stitching Model" in the debug menu. Has associated transform matrices. |
| 3    | Is Skin Model      | Called "Skin Model" in the debug menu. Has associated transform matrices and indexed vertices. |
| 4    | Is Effective Model | Called "Effective Model" in the debug menu. Has physics-driven indexed vertices. |
| 5    | `unused`           |                                                              |
| 6    | `unused`           |                                                              |
| 7    | `unused`           |                                                              |



## Texture Descriptor

GCMFs typically contain an array of texture descriptions that reference textures within the associated TPL file. A GCMF is *not required* to have any texture references. This data type has properties for how that texture should be rendered.

**GameCube FIFO:** This structure is 32-byte aligned to be fed into the GameCube GP. This structure is aligned to `0x20` (32 bytes). There are 12 unused bytes due to this padding.

| Offset | Type                          | Name                 | Description                                                  |
| ------ | ----------------------------- | -------------------- | ------------------------------------------------------------ |
| 0x00   | [`Texture Flags 0x00`]()      | Unknown 0x00         |                                                              |
| 0x02   | [`Texture Mipmap Settings`]() | Mipmap Settings      | Appears to dictate various features for mipmap rendering     |
| 0x03   | [`Texture Wrap Flags`]()      | Wrap Flags           | Flags for how texture should wrap (and more)                 |
| 0x04   | `int16`                       | TPL Texture Index    | Index into TPL for which texture is referenced               |
| 0x06   | [`Texture Flags 0x06`]()      | Unknown 0x06         |                                                              |
| 0x07   | [`Texture Anisotropy`]()      | Anisotropic Level    | Level of anisotropic filtering to use (3 levels)             |
| 0x08   | `uint32`                      | Zero 0x08            | Constant 0                                                   |
| 0x0C   | [`Texture Flags 0x0C`]()      | Unknown 0x0C         |                                                              |
| 0x0D   | `bool`                        | Is Swappable Texture | Boolean that indicates this texture may be swapped out at runtime. This is used for lap-related textures in *<u>F-Zero GX</u>*. |
| 0x0E   | `int16`                       | Index                | Texture index, matches it's zero-indexed value in array      |
| 0x10   | [`Texture Flags 0x10`]()      | Unknown 0x10         |                                                              |



### Texture Flags 0x00 U16

<span style="color:red">[**TODO**: Confirm this is 16 bit]</span>

Appears to affect how the texture UV is used.

| Bit  | Name             | Description                                                  |
| ---- | ---------------- | ------------------------------------------------------------ |
| 0    | `unused`         |                                                              |
| 1    | Enable UV Scroll | When active, textures scroll. This is based on st24 models. Choosing to scroll X/Y seems to be governed by another flag (I'm thinking wrapping mode) |
| 2    | `unused`         |                                                              |
| 3    | Unknown 3        | Don't known. Can be mostly ignored as there are only 7 occurrences total. `(st21,lz.gma, [75,76,77/130] guide_light*, [1/6])` |
| 4    | Unknown 4        | Appears to be used whenever a texture is used for background reflections |
| 5    | Unknown 5        | *No information.*                                            |
| 6    | Unknown 6        | Appears to be used whenever a texture is used for background reflections |
| 7    | `unused`         |                                                              |



### Texture Mipmap Settings U8

| Bit  | Name          | Description                                                  |
| ---- | ------------- | ------------------------------------------------------------ |
| 0    | Enable Mipmap | Enables mipmaps                                              |
| 1    | Unknown 1     | THEORY: when only flag "use custom mipmap". REMARKS: Appears to work in tandem with Unknown 2. |
| 2    | Unknown 2     | See Unknown 1                                                |
| 3    | Enable Near   | Enables `Mipmap NEAR` in texture debugger                    |
| 4    | Unknown 4     | REMARKS: Height map? Blend? These textures are greyscale. Low occurrences: 188 used for tracks and st2 boulders. |
| 5    | Unknown 5     | REMARKS: Used as alpha mask? (likely?) Perhaps some mipmap preservation stuff. |
| 6    | Unknown 6     | REMARKS: 3 occurrences on only MCSO, on a single geometry set. Perhaps error from developers? |
| 7    | Unknown 7     | REMARKS: On many vehicles                                    |



### Texture Wrap Flags U8

| Bit  | Name      | Description                      |
| ---- | --------- | -------------------------------- |
| 0    | Unknown 0 | *No information*                 |
| 1    | Unknown 1 | *No information*                 |
| 2    | Repeat X  | Set UV to repeat along UV.x axis |
| 3    | Mirror X  | Set UV to mirror along UV.x axis |
| 4    | Repeat Y  | Set UV to repeat along UV.y axis |
| 5    | Mirror Y  | Set UV to mirror along UV.y axis |
| 6    | Unknown 6 | *No information*                 |
| 7    | Unknown 7 | *No information*                 |



### Texture Flags 0x06 U8

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | Unknown 0 |             |
| 1    | Unknown 1 |             |
| 2    | Unknown 2 |             |
| 3    | Unknown 3 |             |
| 4    | Unknown 4 |             |
| 5    | Unknown 5 |             |
| 6    | Unknown 6 |             |
| 7    | Unknown 7 |             |



### Texture Anisotropy U8

| Bit  | Name       | Description                |
| ---- | ---------- | -------------------------- |
| 0    | GX Aniso 1 | Set anisotropic level to 1 |
| 1    | GX Aniso 2 | Set anisotropic level to 2 |
| 2    | GX Aniso 4 | Set anisotropic level to 4 |
| 3    | `unused`   |                            |
| 4    | `unused`   |                            |
| 5    | `unused`   |                            |
| 6    | `unused`   |                            |
| 7    | `unused`   |                            |



### Texture Flags 0x0C U8

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | Unknown 0 |             |
| 1    | Unknown 1 |             |
| 2    | Unknown 2 |             |
| 3    | Unknown 3 |             |
| 4    | Unknown 4 |             |
| 5    | Unknown 5 |             |
| 6    | Unknown 6 |             |
| 7    | Unknown 7 |             |



### Texture Flags 0x10 U32

<span style="color:red">[**TODO**: Confirm this is 32 bit]</span>

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | Unknown 0 |             |
| 1    | Unknown 1 |             |
| 2    | Unknown 2 |             |
| 3    | Unknown 3 |             |
| 4    | Unknown 4 |             |
| 5    | Unknown 5 |             |
| 6    | Unknown 6 |             |
| 7    | Unknown 7 |             |



## Transform Matrix Indices 8

This structure could be represented as an `int8` array of fixed length (8). However, as such thing occurs twice throughout these structures, it can be represented as a trivial type as their function appears to be explicit. 

Not all bytes need to point to a matrix in within a `Transform Matrix 3x4`. Null entries are represented as `0xFF` (two's compliment `-1`).



## GCMF Transform Matrices

**GameCube FIFO:** This structure is 32-byte aligned to be fed into the GameCube GP. This structure is of variable size with the base component of size `0x30`. As such, there is padding when there is an uneven number of matrices (pad size of `0x10`) and no padding when even.

| Type                           | Name     | Description                 |
| ------------------------------ | -------- | --------------------------- |
| [Transform Matrix 3x4]() array | Matrices | Array of transform matrices |



### Transform Matrix 3x4

A row-major transform matrix.

| Offset | Type     | Name  | Description      |
| ------ | -------- | ----- | ---------------- |
| 0x00   | `float3` | Row 0 | *No information* |
| 0x0C   | `float3` | Row 1 | *No information* |
| 0x18   | `float3` | Row 2 | *No information* |
| 0x24   | `float3` | Row 3 | *No information* |



## Vertex Control Header

This structure appears to outline various information relating to skinned and effective models. A such, it is ***assumed*** that it is data for controlling the vertices.

**GameCube FIFO:** This structure is 32-byte aligned to be fed into the GameCube GP. This structure is  aligned to `0x20` (32 bytes). There are 12 unused bytes due to this padding.

| Offset | Type    | Name                                   | Description                                     |
| ------ | ------- | -------------------------------------- | ----------------------------------------------- |
| 0x00   | `int32` | Vertex Count                           | Number of vertices                              |
| 0x04   | `int32` | Vertex Control Type 1 Relative Pointer | Relative pointer to Vertex Control Type 1 array |
| 0x08   | `int32` | Vertex Control Type 2 Relative Pointer | Relative pointer to Vertex Control Type 2 array |
| 0x0C   | `int32` | Vertex Control Type 3 Relative Pointer | Relative pointer to Vertex Control Type 3       |
| 0x10   | `int32` | Vertex Control Type 4 Relative Pointer | Relative pointer to Vertex Control Type 4       |



### Vertex Control Type 1

This is more or a less a guess. I haven't spent much time investigating this type.

**Count**: This structure is found in an array. The count is not yet known but can be faked by doing `vertexControlHeader.VertexControlT4RelPtr - vertexControlHeader.VertexControlT1RelPtr` / `0x20` (size of structure). It's hacky but works in the interim.

| Offset | Type     | Name         | Description |
| ------ | -------- | ------------ | ----------- |
| 0x00   | `float3` | Position     |             |
| 0x0C   | `float3` | Normal       |             |
| 0x18   | `uint32` | Unknown 0x18 |             |
| 0x1C   | `float`  | Unknown 0x1C |             |



### Vertex Control Type 2

This layout was taken from: <https://github.com/bobjrsenior/GxUtils/blob/master/GxUtils/LibGxFormat/Gma/GMAFormat.xml>

**Count**: This structure is found in an array. It uses the `count` variable from the [Vertex Control Header](#vertex-control-header).

| Offset | Type                 | Name         | Description |
| ------ | -------------------- | ------------ | ----------- |
| 0x00   | `float3`             | Position     |             |
| 0x0C   | `float3`             | Normal       |             |
| 0x18   | `float2`             | Texture 0 UV |             |
| 0x20   | `float2`             | Texture 1 UV |             |
| 0x28   | `float2`             | Texture 2 UV |             |
| 0x30   | `uint32` / `color32` | Color        |             |
| 0x34   | `uint32`             | Unknown 0x34 |             |
| 0x38   | `uint32`             | Unknown 0x38 |             |
| 0x3C   | `uint32`             | Unknown 0x3C |             |





### Vertex Control Type 3

An array of address arrays.

**Count**: This structure is found in an array. The count is not yet known but can be faked by tentatively reading the `count` component the array until the size is less than 0 or greater than 0x30.

| Offset | Type                | Name      | Description                                                  |
| ------ | ------------------- | --------- | ------------------------------------------------------------ |
| 0x00   | [`Address Array`]() | Addresses | Array of address offsets into [Vertex Control Type 2]() array. |

#### Address Array

| Offset | Type          | Name             | Description                                             |
| ------ | ------------- | ---------------- | ------------------------------------------------------- |
| 0x00   | `int32`       | Count            | Number of elements in Relative Pointer                  |
| 0x04   | `int32` array | Relative Pointer | Relative pointers into [Vertex Control Type 2]() array. |



### Vertex Control Type 4

**Count**: This structure is found in an array. The count is equal to the number of matrices the model has.

| Offset | Type          | Name           | Description                                                  |
| ------ | ------------- | -------------- | ------------------------------------------------------------ |
| 0x00   | `int16` array | Matrix Indices | Index into matrix array. First (root matrix?) entry is always null (`0xFFFF` two's compliment `-1`). |



## GCMF Submesh

A submesh layer. It contains a variety of mesh information governed by properties in the Material and has optionally more information (used for MTX matrix information!?).

| Type                            | Name                      | Description |
| ------------------------------- | ------------------------- | ----------- |
| [`Material`]()                  | Material                  |             |
| [`GX Display List`]()           | Display List 0            |             |
| [`GX Display List`]()           | Display List 1            |             |
| [`Extra Display List Header`]() | Extra Display List Header |             |
| [`GX Display List`]()           | Extra Display List 0      |             |
| [`GX Display List`]()           | Extra Display List 1      |             |



## Material

**GameCube FIFO:** This structure is 32-byte aligned to be fed into the GameCube GP. This structure is aligned to `0x60` (96 bytes). There are 28 unused bytes due to this padding.

| Offset | Type                                  | Name                                  | Description                                                  |
| ------ | ------------------------------------- | ------------------------------------- | ------------------------------------------------------------ |
| 0x00   | `uint16`                              | Zero 0x00                             | Constant 0                                                   |
| 0x02   | [`Material Flags 0x02`]()             | Unknown 0x02                          | Literally only used once for Mat [7/7] on st13 C13_ROAD01. Value is `0x2`, all others `0x0`. See `Unknown 0x3C`. |
| 0x03   | [`Material Flags 0x03`]()             | Unknown 0x03                          |                                                              |
| 0x04   | `uint32` / `ColorRGBA`                | Color 0                               |                                                              |
| 0x08   | `uint32` / `ColorRGBA`                | Color 1                               |                                                              |
| 0x0C   | `uint32` / `ColorRGBA`                | Color 2                               |                                                              |
| 0x10   | [`Material Flags 0x10`]()             | Unknown 0x10                          |                                                              |
| 0x11   | [`Material Flags 0x11`]()             | Unknown 0x11                          |                                                              |
| 0x12   | `int8`                                | Used Material Count                   |                                                              |
| 0x13   | [`Material Vertex Render Flags`]()    | Vertex Render Flags                   |                                                              |
| 0x14   | `int8`                                | Unknown 0x14                          |                                                              |
| 0x15   | [`Material Flags 0x15`]()             | Unknown 0x15                          |                                                              |
| 0x16   | `int16`                               | Texture 0 Index                       |                                                              |
| 0x18   | `int16`                               | Texture 1 Index                       |                                                              |
| 0x1A   | `int16`                               | Texture 2 Index                       |                                                              |
| 0x1C   | [`Material Vertex Attribute Flags`]() | Vertex Descriptor Flags               |                                                              |
| 0x20   | `int8` array, const size 8            | Transform Matrix Specific Indices     |                                                              |
| 0x28   | `int32`                               | Material Display List Size            |                                                              |
| 0x2C   | `int32`                               | Translucid Material Display List Size |                                                              |
| 0x30   | `float3`                              | Bounding Sphere Origin                |                                                              |
| 0x3C   | `float`                               | Unknown 0x3C                          | Literally only used once for Mat [7/7] on st13.gma C13_ROAD01. Value is `1f`, all others `0f`. |
| 0x40   | `uint32`                              | Unknown 0x40                          |                                                              |



### Material Flags 0x02

An *extremely niche* flag (used only once in ~30,000 instances) that when active appears to enable a 32-bit float value at `0x3C` within the Material structure.

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | `unused`  |             |
| 1    | Unknown 1 |             |
| 2    | `unused`  |             |
| 3    | `unused`  |             |
| 4    | `unused`  |             |
| 5    | `unused`  |             |
| 6    | `unused`  |             |
| 7    | `unused`  |             |



### Material Flags 0x03

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | Unknown 0 |             |
| 1    | Unknown 1 |             |
| 2    | Unknown 2 |             |
| 3    | Unknown 3 |             |
| 4    | Unknown 4 |             |
| 5    | Unknown 5 |             |
| 6    | Unknown 6 |             |
| 7    | Unknown 7 |             |



### Material Flags 0x10

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | Unknown 0 |             |
| 1    | Unknown 1 |             |
| 2    | Unknown 2 |             |
| 3    | Unknown 3 |             |
| 4    | Unknown 4 |             |
| 5    | Unknown 5 |             |
| 6    | Unknown 6 |             |
| 7    | Unknown 7 |             |



### Material Flags 0x11

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | Unknown 0 |             |
| 1    | Unknown 1 |             |
| 2    | Unknown 2 |             |
| 3    | Unknown 3 |             |
| 4    | Unknown 4 |             |
| 5    | Unknown 5 |             |
| 6    | Unknown 6 |             |
| 7    | Unknown 7 |             |



### Material Vertex Render Flags 

| Bit  | Name                             | Description |
| ---- | -------------------------------- | ----------- |
| 0    | Render Materials                 |             |
| 1    | Render Translucid Materials      |             |
| 2    | Render Skin or Effective Model A |             |
| 3    | Render Skin or Effective Model B |             |
| 4    | `unused`                         |             |
| 5    | `unused`                         |             |
| 6    | `unused`                         |             |
| 7    | `unused`                         |             |



### Material Flags 0x15

| Bit  | Name      | Description |
| ---- | --------- | ----------- |
| 0    | Unknown 0 |             |
| 1    | Unknown 1 |             |
| 2    | Unknown 2 |             |
| 3    | Unknown 3 |             |
| 4    | `unused`  |             |
| 5    | Unknown 5 |             |
| 6    | Unknown 6 |             |
| 7    | `unused`  |             |



### Material Vertex Attribute Flags

A flag field created using `GXAttr` as the shift count for each flag.

*F-Zero GX* uses 2 `Vertex Attribute Format` (VAF) in the GameCube GX's `Vertex Attribute Table` (VAT). <span style="color:red">[**TODO**: Outline VAF formats and indices in VAT]</span>

| Bit  | Name                | Description                                          |
| ---- | ------------------- | ---------------------------------------------------- |
| 0    | GX_VA_PNMTXIDX      | Position and normal matrix index (used for skinning) |
| 1    | GX_VA_TEX0MTXIDX    | Texture 0 matrix index (used for skinning)           |
| 2    | GX_VA_TEX1MTXIDX    | Texture 1 matrix index (used for skinning)           |
| 3    | GX_VA_TEX2MTXIDX    | Texture 2 matrix index (used for skinning)           |
| 4    | GX_VA_TEX3MTXIDX    | Texture 3 matrix index (used for skinning)           |
| 5    | GX_VA_TEX4MTXIDX    | Texture 4 matrix index (used for skinning)           |
| 6    | GX_VA_TEX5MTXIDX    | Texture 5 matrix index (used for skinning)           |
| 7    | GX_VA_TEX6MTXIDX    | Texture 6 matrix index (used for skinning)           |
| 8    | GX_VA_TEX7MTXIDX    | Texture 7 matrix index (used for skinning)           |
| 9    | GX_VA_POS           | Vertex position                                      |
| 10   | GX_VA_NRM           | Vertex normal                                        |
| 11   | GX_VA_CLR0          | Vertex color 0                                       |
| 12   | GX_VA_CLR1          | Vertex color 1                                       |
| 13   | GX_VA_TEX0          | Input texture coordinate 0                           |
| 14   | GX_VA_TEX1          | Input texture coordinate 1                           |
| 15   | GX_VA_TEX2          | Input texture coordinate 2                           |
| 16   | GX_VA_TEX3          | Input texture coordinate 3                           |
| 17   | GX_VA_TEX4          | Input texture coordinate 4                           |
| 18   | GX_VA_TEX5          | Input texture coordinate 5                           |
| 19   | GX_VA_TEX6          | Input texture coordinate 6                           |
| 20   | GX_VA_TEX7          | Input texture coordinate 7                           |
| 21   | GX_VA_POS_MTX_ARRAY | Position matrix array pointer                        |
| 22   | GX_VA_NRM_MTX_ARRAY | Normal matrix array pointer                          |
| 23   | GX_VA_TEX_MTX_ARRAY | Texture matrix array pointer                         |
| 24   | GX_VA_LIGHT_ARRAY   | Light matrix array pointer                           |
| 25   | GX_VA_NBT           | Vertex normal, bi-normal, and tangent                |
| 26   | `unused`            |                                                      |
| 27   | `unused`            |                                                      |
| 28   | `unused`            |                                                      |
| 29   | `unused`            |                                                      |
| 30   | `unused`            |                                                      |
| 31   | `unused`            |                                                      |



## GX Display List

<span style="color:red">[**TODO**: Complete this section.]</span>

The skinny is that the GameCube GP has a specialized format it expects for graphics data. You should be able to find information about Display Lists fairly easily.

#### GX Display Command

| Offset   | Type                       | Name      | Material                                                     |
| -------- | -------------------------- | --------- | ------------------------------------------------------------ |
| 0x00     | `uint8`                    | Zero 0x00 | Constant 0                                                   |
| 0x01     | `GXPrimitive` + `GXVtxFmt` | Command   | Highest 5 bits are `GXPrimitive` enum and lowest 3 bits are `GXVtxFmt` index. |
| 0x02     | `uint16`?                  | Count     | Number of vertices using Command                             |
| variable | [`GXVertex`]() array       | Vertices  | Vertices array in specified `GXAttr` format                  |

#### GX Vertex

At minimum contains `position`, but can technically have all the data specified in [Material Vertex Attribute Flags]().



## Extra Display List Header

Structure appears after Material's Display Lists if it is a Skin Model or Effective Model.

**GameCube FIFO:** structure is 32-byte aligned to be fed into the GameCube GP. This structure is thusly aligned to `0x20` (32 bytes). There are 16 unused bytes due to this padding.

| Offset | Type      | Name          | Material |
| ------ | --------- | ------------- | -------- |
| 0x00   | `uint32`? | Unknown 0x00  |          |
| 0x04   | `uint32`? | Unknown 0x04  |          |
| 0x08   | `int32`   | Vertex Size 0 |          |
| 0x0C   | `int32`   | Vertex Size 1 |          |

