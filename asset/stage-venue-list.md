# Stage List

This document covers the COLI_COURSE format used in both <u>*F-Zero GX*</u> (`GFZE01`, `GFZJ01`, `GFZP01`) and *<u>F-Zero AX</u>* (`GFZJ8P`).



## Revisions  Table

| Author            | Date       | Version | Description                          |
| ----------------- | ---------- | ------- | ------------------------------------ |
| Raphaël Tétreault | 2019/05/11 | 1.0     | Initial document                     |
| Raphaël Tétreault | 2020/02/07 | 1.1     | Correct "Test Stage" to "Loop Cross" |



## Table Of Contents

[TOC]

# Introduction

## Stage List

| Stage | Venue          | AX                           | GX                       |
| ----- | -------------- | ---------------------------- | ------------------------ |
| 0     | Sand Ocean     | Screw Drive                  | Screw Drive              |
| 1     | Mute City      | Twist Road                   | Twist Road               |
| 2     | Mute City      | -                            | -                        |
| 3     | Mute City      | Serial Gaps                  | Serial Gaps              |
| 4     | Mute City      | -                            | -                        |
| 5     | Aeropolis      | Multiplex                    | Multiplex                |
| 6     | Port Town      | -                            | -                        |
| 7     | Port Town      | Aero Dive                    | Aero Dive                |
| 8     | Lightning      | Loop Cross                   | Loop Cross               |
| 9     | Lightning      | Half Pipe                    | Half Pipe                |
| 10    | Green Plant    | Intersection                 | Intersection             |
| 11    | Green Plant    | Mobius Ring                  | Mobius Ring              |
| 12    | Lightning      | -                            | -                        |
| 13    | Port Town      | Long Pipe                    | Long Pipe                |
| 14    | Big Blue       | Drift Highway                | Drift Highway            |
| 15    | Fire Field     | Cylinder Knot                | Cylinder Knot            |
| 16    | Casino Palace  | Split Oval                   | Split Oval               |
| 17    | Fire Field     | Undulation                   | Undulation               |
| 18    | Fire Field     | -                            | -                        |
| 19    | Outer Space    | -                            | -                        |
| 20    | Outer Space    | -                            | -                        |
| 21    | Aeropolis      | Dragon Slope                 | Dragon Slope             |
| 22    | Cosmo Terminal | -                            | -                        |
| 23    | Lightning      | -                            | -                        |
| 24    | Cosmo Terminal | Trident                      | Trident                  |
| 25    | Sand Ocean     | Lateral Shift                | Lateral Shift            |
| 26    | Sand Ocean     | Surface Slide                | Surface Slide            |
| 27    | Big Blue       | Ordeal                       | Ordeal                   |
| 28    | Phantom Road   | Slim-Line Slits              | Slim-Line Slits          |
| 29    | Casino Palace  | Double Branches              | Double Branches          |
| 30    | Sand Ocean     | -                            | -                        |
| 31    | Aeropolis      | Screw Drive                  | Screw Drive              |
| 32    | Outer Space    | Meteor Stream                | Meteor Stream            |
| 33    | Port Town      | Cylinder Wave                | Cylinder Wave            |
| 34    | Lightning      | Thunder Road                 | Thunder Road             |
| 35    | Green Plant    | Spiral                       | Spiral                   |
| 36    | Mute City      | Sonic Oval                   | Sonic Oval               |
| 37    | Mute City      | Story 1                      | Story 1                  |
| 38    | Sand Ocean     | Story 2                      | Story 2                  |
| 39    | Casino Palace  | Story 3                      | Story 3                  |
| 40    | Big Blue       | Story 4                      | Story 4                  |
| 41    | Lightning      | Story 5                      | Story 5                  |
| 42    | Port Town      | Story 6                      | Story 6                  |
| 43    | Mute City      | Story 7                      | Story 7                  |
| 44    | Fire Field     | Story 8                      | Story 8                  |
| 45    | Phantom Road   | Story 9                      | Story 9                  |
| 46    | -              | -                            | -                        |
| 47    | -              | -                            | -                        |
| 48    | -              | -                            | -                        |
| 49    | Win GX         | Grand Prix Podium            | Grand Prix Podium        |
| 50    | Win            | Victory Lap (Sonic Oval)     | Victory Lap (Sonic Oval) |
| 51    | -              | -                            | -                        |
| 52    | -              | -                            | -                        |
| 53    | -              | -                            | -                        |
| 54    | -              | -                            | -                        |
| 55    | -              | -                            | -                        |
| 56    | -              | -                            | -                        |
| 57    | -              | -                            | -                        |
| 58    | -              | -                            | -                        |
| 59    | -              | -                            | -                        |
| 60    | -              | -                            | -                        |
| 61    | -              | -                            | -                        |
| 62    | -              | -                            | -                        |
| 63    | -              | -                            | -                        |
| 64    | -              | -                            | -                        |
| 65    | -              | -                            | -                        |
| 66    | -              | -                            | -                        |
| 67    | -              | -                            | -                        |
| 68    | -              | -                            | -                        |
| 69    | -              | -                            | -                        |
| 70    | -              | -                            | -                        |
| 71    | -              | -                            | -                        |
| 72    | -              | Surface Slide                | -                        |
| 73    | -              | -                            | -                        |
| 74    | -              | -                            | -                        |
| 75    | -              | -                            | -                        |
| 76    | -              | -                            | -                        |
| 77    | -              | [Loop Cross [1]](#Citations) | -                        |
| 78    | -              | -                            | -                        |
| 79    | -              | -                            | -                        |
| 80    | -              | -                            | -                        |
| 81    | -              | -                            | -                        |
| 82    | -              | -                            | -                        |
| 83    | -              | -                            | -                        |
| 84    | -              | -                            | -                        |
| 85    | -              | -                            | -                        |
| 86    | -              | Meteor Stream                | -                        |
| 87    | -              | Cylinder Wave                | -                        |
| 88    | -              | -                            | -                        |
| 89    | -              | -                            | -                        |
| 90    | -              | Long Pipe                    | -                        |
| 91    | -              | Story 2                      | -                        |
| 92    | -              | Story 3                      | -                        |
| 93    | -              | Story 4                      | -                        |
| 94    | -              | Story 5                      | -                        |
| 95    | -              | Story 6                      | -                        |
| 96    | -              | Story 7                      | -                        |
| 97    | -              | Story 8                      | -                        |
| 98    | -              | Story 9                      | -                        |
| 99    | -              | -                            | -                        |
| 100   | -              | -                            | -                        |
| 101   | -              | Twist Road                   | -                        |
| 102   | -              | Twist Road                   | -                        |
| 103   | -              | Multiplex                    | -                        |
| 104   | -              | Intersection                 | -                        |
| 105   | -              | Undulation                   | -                        |
| 106   | -              | -                            | -                        |
| 107   | -              | Drift Highway                | -                        |
| 108   | -              | Aero Dive                    | -                        |
| 109   | -              | -                            | -                        |
| 110   | -              | Lateral Shift                | -                        |

### Citations

[1] Stage 77 "Loop Cross" appears to have been serialized using an older method as it loads incorrectly in-game but some data structures can still be inspected properly using custom tools, namely Manifold.

## Indexing Stages

The code used to name and load courses uses a string format searching for 2 digits. Thus, a courses' name will be:

* Single digit: COLI_COURSE00, *01, *02...
* Double digit: COLI_COURSE10, *11, *12...
* Three or more digits: COLI_COURSE100, *101, *102...

Indexing is done using a byte and so a theoretical maximum of 256 stages are available. It is not yet confirmed if index 256 (0xFF) is used for null termination in this regard.